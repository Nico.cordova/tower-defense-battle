﻿using System.Collections.Generic;
using UnityEngine;

public class AlteredState
{

    public float duration = 0f;
    public Debuff[] modifiers;
    
    private float timeToFinish;

    public void ReApply()
    {
        timeToFinish = duration;
    }
}
