﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat {

    [SerializeField]
    private float baseValue = 0f;
    [SerializeField]
    private float levelGain = 0f;

    private List<float> modifiers = new List<float>();
    
    public float GetValue()
    {
        float finalValue = baseValue;
        modifiers.ForEach(x => finalValue += x);
        return finalValue;
    }

    public float GetLevelGain()
    {
        return levelGain;
    }

    public float GetBaseValue()
    {
        return baseValue;
    }

    public void AddModifier (float modifier)
    {
        if (modifier != 0)
            modifiers.Add(modifier);
    }

    public void RemoveModifier (float modifier)
    {
        if (modifier != 0)
            modifiers.Remove(modifier);
    }

    public void EmptyModifiers()
    {
        modifiers.Clear();
    }
}
