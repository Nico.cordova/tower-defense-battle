﻿using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    [Header("Base")]
    public float maxHealth = 100;
    public Stat moveSpeed;
    private float currentHealth;

    [Header("Level")]
    public int level = 1;
    public int experience;

    [Header("Attack")]
    public Stat damage;
    public Stat attackCooldown;

    [Header("Defense")]
    public Stat fireResistance;
    public Stat waterResistance;
    public Stat electricResistance;
    public Stat windResistance;
    public Stat earthResistance;


    void Awake()
    {
        currentHealth = maxHealth;
    }

    public float TakeDamage(float damage)
    {
        currentHealth -= damage;

        return currentHealth;
    }

    public float GetCurrentHealth()
    {
        return currentHealth;
    }

    public virtual void Die() {
        
    }
}
