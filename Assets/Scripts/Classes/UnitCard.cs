﻿using UnityEngine;
[CreateAssetMenu(fileName = "New Card", menuName = "Unit Card")]
public class UnitCard : ScriptableObject
{

    public string unitName;
    public float attackDamage;
    public float moveSpeed;
    public float health;
    public float fireDefense;
    public float waterDefense;
    public float electricDefense;
    public float windDefense;
    public float earthDefense;
    public Sprite picture;
}
