﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Debuff
{

    [SerializeField]
    private float amountToModify = 0f;
    [SerializeField]
    private Stat statToModify = null;

    public float GetModifier()
    {
        return amountToModify;
    }

    public Stat GetStatToModify()
    {
        return statToModify;
    }
}
