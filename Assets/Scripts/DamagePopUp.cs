﻿using UnityEngine;
using TMPro;
using System.Collections.Specialized;

public class DamagePopUp : MonoBehaviour
{
    private TextMeshPro textMesh;

    public static DamagePopUp Create(Vector3 position, int damageAmount, Color32 color)
    {
        if (BuildManager.instance.damagePopPrefab)
        {
            GameObject damagePopupGO = Instantiate(BuildManager.instance.damagePopPrefab, position, Quaternion.identity);
            DamagePopUp damagePopup = damagePopupGO.GetComponentInChildren<DamagePopUp>();
            damagePopup.Setup(damageAmount, color);
            return damagePopup;
        } else
        {
            return null;
        }
        
    }


    private void Awake()
    {
        textMesh = transform.GetComponent<TextMeshPro>();
    }

    public void Setup(int damageAmount, Color32 color)
    {
        textMesh.SetText(damageAmount.ToString());
        textMesh.color = color;
    }
}
