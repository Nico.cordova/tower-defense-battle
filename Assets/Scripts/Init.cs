﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Init : MonoBehaviour
{
    public Transform target;
    public Pathfinding.AIDestinationSetter aIDestinationSetter;
    // Start is called before the first frame update
    void Start()
    {
        var script = GetComponent("AIDestinationSetter");
        aIDestinationSetter.target = target;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
