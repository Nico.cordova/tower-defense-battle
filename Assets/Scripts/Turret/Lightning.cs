﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightning : MonoBehaviour
{
    public float strokeTime = 0.5f;
    public float initialDamage = 10f;

    private float attackDamage;
    private ElementalDamage[] elementalDistribution;
    private LineRenderer lineRenderer;
    public static Lightning Create(GameObject bulletPrefab, Vector3 position, int damage, ElementalDamage[] p_elementalDistribution)
    {
        GameObject lightningGO = Instantiate(bulletPrefab, position, Quaternion.identity);
        Lightning lightning = lightningGO.GetComponentInChildren<Lightning>();
        lightning.Setup(damage, p_elementalDistribution);
        return lightning;
    }

    private void Setup(int damage, ElementalDamage[] p_elementalDistribution)
    {
        attackDamage = damage;
        elementalDistribution = p_elementalDistribution;
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
    }


    public void createBetween(Transform from, Transform target)
    {
        if (target)
        {
            if (!lineRenderer.enabled)
            {
                lineRenderer.enabled = true;
            }
            lineRenderer.SetPosition(0, from.position);
            lineRenderer.SetPosition(1, target.position);
            Damage(target);
            StartCoroutine(LateCall());
        }
    }

    private void Damage(Transform target)
    {
        EnemyController targetController = target.GetComponent<EnemyController>();
        targetController.TakeDamage(attackDamage, elementalDistribution);
    }

    IEnumerator LateCall()
    {
        yield return new WaitForSeconds(strokeTime);
        lineRenderer.enabled = false;
    }

    public void hide()
    {
        lineRenderer.enabled = false;
    }
}
