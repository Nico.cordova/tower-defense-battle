﻿using UnityEngine.UI;
using UnityEngine;

public class TurretBoardController : MonoBehaviour
{
    [Header("Tabs")]
    public GameObject StatsTab;
    public GameObject LvlUpTab;
    public GameObject DefaultTab;

    [Header("Texts")]
    public Text AttackText;
    public Text AttackRateText;
    public Text TargetsText;

    [Header("Icons")]
    public SpriteRenderer ElementRenderer;
    public SpriteRenderer Gem1;
    public SpriteRenderer Gem2;
    public SpriteRenderer Gem3;

    private GameObject ActiveTab;
    private GameObject SelectedTurret;
    private BuildManager buildManager;
    private void Start()
    {
        buildManager = BuildManager.instance;
        ActiveTab = StatsTab;
        ActiveTab.SetActive(true);
    }

    public void InitFromTurret(GameObject turret)
    {
        SelectedTurret = turret;
        Turret turretController = turret.GetComponent<Turret>();
        TurretStats turretStats = turret.GetComponent<TurretStats>();
        AttackText.text = turretStats.GetDamage().ToString();
        AttackRateText.text = turretController.fireRate.ToString();
        if (turretController.multiTarget)
        {
            TargetsText.text = turretController.maxTargets.ToString();
        } else
        {
            TargetsText.text = "1";
        }
        if (turretStats.GemLvl1.GemSprite != null)
        {
            Gem1.sprite = turretStats.GemLvl1.GemSprite;
        }
        if (turretStats.GemLvl2.GemSprite != null)
        {
            Gem2.sprite = turretStats.GemLvl2.GemSprite;
        }
        if (turretStats.GemLvl3.GemSprite != null)
        {
            Gem3.sprite = turretStats.GemLvl3.GemSprite;
        }
        ElementRenderer.sprite = turretController.ElementStone;
    }

    public void UpgradeTurretClick() 
    {
        SetActiveTab(LvlUpTab);
    }

    public void SellTurretClick()
    {
        Turret turretController = SelectedTurret.GetComponent<Turret>();
        turretController.Sell();
        SelectedTurret = null;
        buildManager.DestroyActiveTurretGui();
    }

    private bool canUpgrade(GemBlueprint gem)
    {
        TurretStats turretStats = SelectedTurret.GetComponent<TurretStats>();

        if (PlayerStats.Money.FireCurrency >= gem.Cost.FireCurrency
            && PlayerStats.Money.WaterCurrency >= gem.Cost.WaterCurrency
            && PlayerStats.Money.EarthCurrency >= gem.Cost.EarthCurrency
            && PlayerStats.Money.WindCurrency >= gem.Cost.WindCurrency
            && PlayerStats.Money.ElectricCurrency >= gem.Cost.ElectricCurrency
            && PlayerStats.Health >= gem.Cost.HealthCurrency
            && turretStats.GetActualExperience() == turretStats.experienceToUpgrade)
        {
            return true;
        }

        return false;
    }

    public void UpgradeWater()
    {
        Debug.Log("UpgradeWater()");
        if (canUpgrade(buildManager.waterGem)) 
        {
            DeliverGem(buildManager.waterGem);
        }
    }
    public void UpgradeWind()
    {
        Debug.Log("UpgradeWind()");
        if (canUpgrade(buildManager.windGem))
        {
            DeliverGem(buildManager.windGem);
        }
    }
    public void UpgradeFire()
    {
        Debug.Log("UpgradeFire()");
        if (canUpgrade(buildManager.fireGem))
        {
            DeliverGem(buildManager.fireGem);
        }
    }
    public void UpgradeEarth()
    {
        Debug.Log("UpgradeEarth()");
        if (canUpgrade(buildManager.earthGem))
        {
            DeliverGem(buildManager.earthGem);
        }
    }
    public void UpgradeElectric()
    {
        Debug.Log("UpgradeElectric()");
        if (canUpgrade(buildManager.electricGem))
        {
            DeliverGem(buildManager.electricGem);
        }
    }

    public void DeliverGem(GemBlueprint gem)
    {
        TurretStats turretStats = SelectedTurret.GetComponent<TurretStats>();
        turretStats.LevelUp();
        ChargePlayer(gem);
        turretStats.EquipGem(gem.Gem);
        buildManager.DestroyActiveTurretGui();
    }

    private void ChargePlayer(GemBlueprint gem)
    {
        if (gem.Cost.FireCurrency >= 0)
        {
            PlayerStats.Money.FireCurrency -= gem.Cost.FireCurrency;
        }
        if (gem.Cost.WaterCurrency >= 0)
        {
            PlayerStats.Money.WaterCurrency -= gem.Cost.WaterCurrency;
        }
        if (gem.Cost.WindCurrency >= 0)
        {
            PlayerStats.Money.WindCurrency -= gem.Cost.WindCurrency;
        }
        if (gem.Cost.ElectricCurrency >= 0)
        {
            PlayerStats.Money.ElectricCurrency -= gem.Cost.ElectricCurrency;
        }
        if (gem.Cost.EarthCurrency >= 0)
        {
            PlayerStats.Money.EarthCurrency -= gem.Cost.EarthCurrency;
        }
        TurretStats turretStats = SelectedTurret.GetComponent<TurretStats>();
        turretStats.ResetExperience();

    }

    public void SetActiveTab(GameObject Tab)
    {
        ActiveTab.SetActive(false);
        Tab.SetActive(true);
        ActiveTab = Tab;
    }

}