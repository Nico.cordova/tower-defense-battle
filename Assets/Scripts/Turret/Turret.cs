﻿using System;
using System.Collections;
using UnityEngine;

public class Turret : MonoBehaviour
{
    [Header("Use Bullets (default)")]
    private float fireCountdown;
    public float fireRate = 1f;
    public GameObject bulletPrefab;

    [Header("Use Lightning")]
    public bool useLaser = false;
    public GameObject lightningPrefab;

    [Header("Multi-Target")]
    public bool multiTarget = false;
    public int maxTargets = 4;

    [Header("Setup")]
    public string enemyTag;
    public Transform pointer;
    public Transform firePoint;
    public float turnSpeed = 10f;
    public Sprite ElementStone;

    public Transform[] targets;
    public Lightning[] lightnings;
    [SerializeField]
    private TurretStats stats;
    // Start is called before the first frame update

    private bool hasTargets = false;
    
    public void Sell()
    {
        Destroy(gameObject);
    }
    
    void Start()
    {
        stats = gameObject.GetComponent<TurretStats>();
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
        if (multiTarget)
        {
            targets = new Transform[maxTargets];
        }
        else
        {
            targets = new Transform[1];
        }

        if (useLaser && multiTarget)
        {
            lightnings = new Lightning[maxTargets];
            for (int i = 0; i < maxTargets; i++)
            {
                lightnings.SetValue(GetLightning(), i);
            }
        } else if (useLaser)
        {
            lightnings = new Lightning[1];
            lightnings.SetValue(GetLightning(), 0);
        }

        

    }

    void UpdateTarget()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll((Vector2)transform.position, stats.range);
        if (multiTarget)
        {
            Array.Clear(targets, 0, maxTargets);
            int n_targets = 0;
            foreach (Collider2D collider in colliders)
            {

                if (collider.tag == enemyTag && n_targets < maxTargets)
                {
                    targets.SetValue(collider.gameObject.transform, n_targets);
                    n_targets ++;
                }
            }
            if (n_targets != 0)
            {
                hasTargets = true; 
            } 

        } else
        {
            float shortestDistance = Mathf.Infinity;
            GameObject nearestEnemy = null;

            foreach (Collider2D collider in colliders)
            {
                if (collider.tag == enemyTag)
                {
                    float distanceToEnemy = Vector2.Distance(transform.position, collider.transform.position);

                    if (distanceToEnemy < shortestDistance)
                    {
                        shortestDistance = distanceToEnemy;
                        nearestEnemy = collider.gameObject;
                    }
                }
            }

            if (nearestEnemy != null)
            {
                targets.SetValue(nearestEnemy.transform, 0);
                PointToTarget();
                hasTargets = true;
            }
        }
    }

    void PointToTarget()
    {
        Vector2 dir = targets[0].position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion lookRotation = Quaternion.AngleAxis(angle, Vector3.forward);
        Quaternion n_rotation = Quaternion.Lerp(pointer.rotation, lookRotation, Time.deltaTime * turnSpeed);
        pointer.rotation = n_rotation;
    }

    void Laser()
    {
        for (int i = 0; i < targets.Length; i++)
        {
            lightnings[i].createBetween(firePoint,targets[i]);
        }
    }

    private Lightning GetLightning()
    {
        Debug.Log(stats.GetDamage());
        Debug.Log(stats.GetElementalDamages());
        Lightning newLightning = Lightning.Create(lightningPrefab,transform.position, stats.GetDamage(), stats.GetElementalDamages());
        return newLightning;
    }

    void Shoot()
    {
        if (!multiTarget)
        {

            //GameObject bulletGO = Instantiate<GameObject>(bulletPrefab, firePoint.position, firePoint.rotation);
            Bullet bullet = Bullet.Create(bulletPrefab, firePoint.position, stats.GetDamage(), stats.GetElementalDamages());

            if (bullet != null)
                bullet.Seek(targets[0]);
        }
    }

 
    // Update is called once per frame
    void Update()
    {
        if (!hasTargets)
            return;

        if (useLaser)
        {
            if (fireCountdown <= 0f)
            {
                Laser();
                fireCountdown = 1f / fireRate;
            }
            fireCountdown -= Time.deltaTime;

        } else
        {
            if (fireCountdown <= 0f)
            {
                Shoot();
                fireCountdown = 1f / fireRate;
            }
            fireCountdown -= Time.deltaTime;
        }
        

    }
    
}
