﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Gem
{
    [SerializeField]
    public Sprite GemSprite;
    [SerializeField]
    public string ElementType;
}
