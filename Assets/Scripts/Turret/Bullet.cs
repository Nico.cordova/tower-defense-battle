﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float explosionRadius = 0f;
    public float speed = 70f;
    public int initialDamage = 40;
    public GameObject impactEffect;

    private int attackDamage = 0;
    private ElementalDamage[] elementalDistribution;
    private Transform target;
    private Rigidbody2D rb;
    public static Bullet Create(GameObject bulletPrefab, Vector3 position, int damage, ElementalDamage[] p_elementalDistribution)
    {
        GameObject bulletGO = Instantiate(bulletPrefab, position, Quaternion.identity);
        Bullet bullet = bulletGO.GetComponentInChildren<Bullet>();
        bullet.Setup(damage, p_elementalDistribution);
        return bullet;
    }

    private void Setup(int damage, ElementalDamage[] p_elementalDistribution)
    {
        
        attackDamage = damage;
        elementalDistribution = p_elementalDistribution;
    }

    public void Start()
    {
        if (attackDamage == 0){
            attackDamage = initialDamage;
        }
        rb = this.GetComponent<Rigidbody2D>();
    }
    public void Seek(Transform _target)
    {
        target = _target;
    }

    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector2 dir = target.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        rb.rotation = angle;

        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        };

        transform.Translate(dir.normalized * distanceThisFrame,Space.World);

    }

    void Explode()
    {  
        Collider2D[] colliders =  Physics2D.OverlapCircleAll((Vector2)transform.position, explosionRadius);
        foreach (Collider2D collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                DamageEnemy(collider.transform);
            }
        }
    }

    private void HitTarget()
    {
        GameObject effectInst = Instantiate<GameObject>(impactEffect, transform.position, impactEffect.transform.rotation);
        if (explosionRadius > 0f)
        {
            Explode();
        } else
        {
            DamageEnemy(target);
        }

        Destroy(effectInst, 0.8f);
        Destroy(gameObject);
    }

    private void DamageEnemy (Transform enemy)
    {
        EnemyController targetController = enemy.GetComponent<EnemyController>();
        targetController.TakeDamage(attackDamage, elementalDistribution);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
