﻿using UnityEngine;
using System.Collections;

public class TurretStats : MonoBehaviour
{

    [Header("Level")]
    public int startingLevel = 0;
    public int startingExperience;
    public int experienceToUpgrade = 100;

    [Header("Gems")]
    public Gem GemLvl1;
    public Gem GemLvl2;
    public Gem GemLvl3;

    [Header("General")]
    public float range = 6f;
    public int initialDamage;
    public ElementalDamage[] initialElementalDistribution;

    private int level;
    private int experience = 0;
    private int damage;
    private ElementalDamage[] elementalDistribution;
    private void Awake()
    {

        level = startingLevel;
        elementalDistribution = initialElementalDistribution;
        damage = initialDamage;
        Debug.Log("DAMAGE SET: ");
        Debug.Log(damage);
    }

    public void GainExperience(int gainedAmount)
    {
        if ( experience <= experienceToUpgrade )
        {
            experience += gainedAmount;
        }
    }

    public int GetDamage()
    {
        return damage;
    }

    public ElementalDamage[] GetElementalDamages()
    {
        return elementalDistribution;
    }
    public int GetActualLevel()
    {
        return level;
    }

    public int GetActualExperience()
    {
        return experience;
    }

    public void ResetExperience()
    {
        experience = 0;
    }

    public void LevelUp()
    {
        experience -= experienceToUpgrade;
        level += 1;
    }

    public void EquipGem(Gem gem)
    {
        switch (level)
        {
            case 1:
                GemLvl1 = gem;
                break;
            case 2:
                GemLvl2 = gem;
                break;
            case 3:
                GemLvl3 = gem;
                break;
            default:
                break;
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

}
