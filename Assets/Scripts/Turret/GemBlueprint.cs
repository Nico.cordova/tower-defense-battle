﻿using UnityEngine;

[System.Serializable]
public class GemBlueprint {
    public Gem Gem;
    public Currency Cost;
}
