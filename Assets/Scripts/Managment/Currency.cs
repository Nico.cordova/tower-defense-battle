﻿[System.Serializable]
public class Currency
{
    public int FireCurrency;
    public int WaterCurrency;
    public int WindCurrency;
    public int ElectricCurrency;
    public int EarthCurrency;
    public int HealthCurrency;
}