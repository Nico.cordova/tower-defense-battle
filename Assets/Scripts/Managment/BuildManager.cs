﻿using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;

    [Header("Turrets")]
    public TurretBlueprint waterTurret;
    public TurretBlueprint fireTurret;
    public TurretBlueprint electricTurret;
    public TurretBlueprint windTurret;
    public TurretBlueprint earthTurret;

    [Header("Gems")]

    public GemBlueprint waterGem;
    public GemBlueprint fireGem;
    public GemBlueprint electricGem;
    public GemBlueprint windGem;
    public GemBlueprint earthGem;

    [Header("Graphical interfaces")]
    public GameObject buildingGUI;
    public GameObject turretGUI;
    public GameObject damagePopPrefab;
    public Color32 normalDamageColor;
    public Color32 effectiveDamageColor;
    public Color32 reducedDamageColor;
    public Color32 healColor;

    private GameObject activeGUI;
    private GameObject activeTurretGUI;
    private bool guiOnScene = false;
    private bool turretGuiOnScene = false;
    Vector3 touchPosWorld;
    //Change me to change the touch phase used.
    TouchPhase touchPhase = TouchPhase.Ended;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one buildmanager in scene!");
            return;
        }
        instance = this;
    }



    void Update()
    {
        //We check if we have more than one touch happening.
        //We also check if the first touches phase is Ended (that the finger was lifted)
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == touchPhase)
        {
            //We transform the touch position into word space from screen space and store it.
            touchPosWorld = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);

            Vector2 touchPosWorld2D = new Vector2(touchPosWorld.x, touchPosWorld.y);

            //We now raycast with this information. If we have hit something we can process it.
            RaycastHit2D hitInformation = Physics2D.Raycast(touchPosWorld2D, Camera.main.transform.forward);

            if (hitInformation.collider != null)
            {
                //We should have hit something with a 2D Physics collider!
                GameObject touchedObject = hitInformation.transform.gameObject;
                //touchedObject should be the object someone touched.
                Debug.Log("Touched " + touchedObject.tag);
                if (touchedObject.tag == "BuildingNode")
                {
                    BuildingNode buildingNode = touchedObject.GetComponent<BuildingNode>();
                    buildingNode.LaunchShop();
                }
                
            }
        }
    }
    public bool InterfaceOnScene()
    {
        return guiOnScene;
    }

    public bool turretInterfaceOnScene()
    {
        return turretGuiOnScene;
    }

    public GameObject GetActiveGui()
    {
        return activeGUI;
    }

    public void DestroyActiveGui()
    {
        Destroy(activeGUI);
        guiOnScene = false;
    }

    public GameObject GetActiveTurretGui()
    {
        return activeTurretGUI;
    }

    public void DestroyActiveTurretGui()
    {
        Destroy(activeTurretGUI);
        turretGuiOnScene = false;
    }

    public GameObject GetGui(Vector3 position, Quaternion rotation)
    {
        if (turretGuiOnScene)
        {
            DestroyActiveTurretGui();
        }
        GameObject guiObj;
        if (guiOnScene)
        {
            Debug.Log("GUI Existe! Moviendo!");
            guiObj = activeGUI;
            guiObj.transform.position = position;
        }
        else
        {
            Debug.Log("GUI NO Existe! Creando!");
            GameObject buildingGUI1 = buildingGUI;
            guiObj = (GameObject)Instantiate(buildingGUI1, position, rotation);
        }
        
        activeGUI = guiObj;
        guiOnScene = true;
        return guiObj;
    }

    public GameObject GetTurretGui(Vector3 position, Quaternion rotation)
    {
        if (guiOnScene)
        {
            DestroyActiveGui();
        }
        GameObject guiObj;
        if (turretGuiOnScene)
        {
            Debug.Log("GUI Existe! Moviendo!");
            guiObj = activeTurretGUI;
            guiObj.transform.position = position;
        }
        else
        {
            Debug.Log("GUI NO Existe! Creando!");
            guiObj = (GameObject)Instantiate(turretGUI, position, rotation);
        }

        activeTurretGUI = guiObj;
        turretGuiOnScene = true;
        return guiObj;
    }
}
