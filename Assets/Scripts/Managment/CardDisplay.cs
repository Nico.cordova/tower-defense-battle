﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class CardDisplay : MonoBehaviour
{
    // Start is called before the first frame update
    public EnemyStats unitStats;

    public TextMeshProUGUI healthText;
    public TextMeshProUGUI attackDamageText;
    public TextMeshProUGUI moveSpeedText;
    public TextMeshProUGUI fireDefenseText;
    public TextMeshProUGUI waterDefenseText;
    public TextMeshProUGUI electricDefenseText;
    public TextMeshProUGUI windDefenseText;
    public TextMeshProUGUI earthDefenseText;
    public Image unitDisplay;
    
    void Start()
    {
        healthText.text = unitStats.maxHealth.ToString();
        attackDamageText.text = unitStats.damage.GetValue().ToString();
        moveSpeedText.text = unitStats.moveSpeed.GetValue().ToString();
        fireDefenseText.text = unitStats.fireResistance.GetValue().ToString();
        waterDefenseText.text = unitStats.waterResistance.GetValue().ToString();
        electricDefenseText.text = unitStats.electricResistance.GetValue().ToString();
        windDefenseText.text = unitStats.windResistance.GetValue().ToString();
        earthDefenseText.text = unitStats.earthResistance.GetValue().ToString();
        unitDisplay.sprite = unitStats.displayImage;
    }
}
