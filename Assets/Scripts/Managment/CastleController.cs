﻿using UnityEngine;

public class CastleController : MonoBehaviour
{
    PlayerStats playerStats;

    private void Start()
    {
        playerStats = PlayerStats.instance;
    }
    public void DamagePlayer(float amount)
    {
        playerStats.TakeDamage(amount);
    }

}
