﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    BuildManager buildManager;
    private GameObject nodeToBuildIn;
    private TurretBlueprint selectedTurret;
    private void Start()
    {
        buildManager = BuildManager.instance;
    }
    public void PurchaseWaterTurret()
    {
        selectedTurret = buildManager.waterTurret;
        sellTurret();
    }

    public void PurchaseFireTurret()
    {
        selectedTurret = buildManager.fireTurret;
        sellTurret();
    }

    public void PurchaseElectricTurret()
    {
        selectedTurret = buildManager.electricTurret;
        sellTurret();
    }

    public void PurchaseWindTurret()
    {
        selectedTurret = buildManager.windTurret;
        sellTurret();
    }

    public void PurchaseEarthTurret()
    {
        selectedTurret = buildManager.earthTurret;
        sellTurret();
    }

    private void sellTurret()
    {
        if (!canBuy(selectedTurret))
        {
            Debug.Log("No tienes suficiente dinero para eso!");
            return;
        }

        takeMoney(selectedTurret);
        deliverTurret(selectedTurret);
        buildManager.DestroyActiveGui();
    }
        
    public void SetNodeToBuildIn(GameObject node)
    {
        nodeToBuildIn = node;
    }

    public static bool canBuy(TurretBlueprint turret)
    {
        if ( PlayerStats.Money.FireCurrency >= turret.cost.FireCurrency 
            && PlayerStats.Money.WaterCurrency >= turret.cost.WaterCurrency
            && PlayerStats.Money.EarthCurrency >= turret.cost.EarthCurrency
            && PlayerStats.Money.WindCurrency >= turret.cost.WindCurrency
            && PlayerStats.Money.ElectricCurrency >= turret.cost.ElectricCurrency
            && PlayerStats.Health >= turret.cost.HealthCurrency)
        {
           return true;
        }
        
        return false;
    }

    private void takeMoney(TurretBlueprint turret)
    {
        if (turret.cost.FireCurrency >= 0)
        {
            PlayerStats.Money.FireCurrency -= turret.cost.FireCurrency;
        }
        if (turret.cost.WaterCurrency >= 0)
        {
            PlayerStats.Money.WaterCurrency -= turret.cost.WaterCurrency;
        }
        if (turret.cost.WindCurrency >= 0)
        {
            PlayerStats.Money.WindCurrency -= turret.cost.WindCurrency;
        }
        if (turret.cost.ElectricCurrency >= 0)
        {
            PlayerStats.Money.ElectricCurrency -= turret.cost.ElectricCurrency;
        }
        if (turret.cost.EarthCurrency >= 0)
        {
            PlayerStats.Money.EarthCurrency -= turret.cost.EarthCurrency;
        }
    }

    private void deliverTurret(TurretBlueprint turretSold)
    {
        BuildingNode nodeController = nodeToBuildIn.GetComponent<BuildingNode>();
        nodeController.BuildTurret(turretSold);
    }
}
