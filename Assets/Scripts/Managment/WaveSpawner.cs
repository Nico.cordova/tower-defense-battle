﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
public class WaveSpawner : MonoBehaviour
{
    
    public Transform spawnPoint;
    public Transform enemyTarget;

    public TextMeshProUGUI waveTimerText;
    public TextMeshProUGUI waveNumberText;
    public float timeBetweenWaves = 5f;
    private float countdown = 2f;

    public float timeBetweenSpawn = 0.9f;
    private int waveNumber = 0;
    private Queue<GameObject> _enemyQueue;
    private int enemies = 0;
    private bool finishedSpawning;

    private void Start()
    {
        finishedSpawning = false;
    }
    void Update()
    {
        if (countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves;
        }

        countdown -= Time.deltaTime;
        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);

        waveTimerText.text = Mathf.Floor(countdown).ToString();
    }

    IEnumerator SpawnWave()
    {
        waveNumber++;
        waveNumberText.text = waveNumber.ToString();
        PlayerStats.Rounds++;
        for (int i = 0; i < waveNumber; i++)
        {
            SpawnEnemy();
            //AstarPath.active.Scan();
            yield return new WaitForSeconds(timeBetweenSpawn);
        }
    }

    public bool FinishedSpawning()
    {
        return finishedSpawning;
    }
    void SpawnEnemy()
    {
        GameObject enemy = GetEnemy();
        if (enemy)
        {
            enemy.name = "enemy_" + enemies;
            EnemyAI setter = enemy.GetComponent<EnemyAI>();
            setter.target = enemyTarget;
            enemies++;
        }
        else {
            finishedSpawning = true;
        }
    }

    private GameObject GetEnemy()
    {
        if (_enemyQueue.Count > 0)
        {
            return Instantiate<GameObject>(_enemyQueue.Dequeue(), spawnPoint.position, spawnPoint.rotation);
        }
        else 
        { 
            return null;
        }
    }

    public void AddEnemyQueue(GameObject[] dices)
    {
        _enemyQueue = new Queue<GameObject>(dices);
    }
}
