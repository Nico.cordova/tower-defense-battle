﻿using UnityEngine;
using UnityEngine.AI;

internal class Begin : IState
{
    private readonly GameObject _gameManager;

    public Begin(GameObject gameManager)
    {
        _gameManager = gameManager;
    }
    public void Tick()
    {
    }

    public void OnEnter()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject enemy in enemies)
            GameObject.Destroy(enemy);
    }

    public void OnExit()
    {
    }
}