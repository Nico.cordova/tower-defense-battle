﻿using UnityEngine;
using UnityEngine.AI;

internal class WavePick : IState
{
    private readonly GameObject _gameManager;
    public bool lockedPick;
    public WavePick(GameObject gameManager)
    {
        _gameManager = gameManager;
        lockedPick = false;
    }
    public void Tick()
    {
    }

    public void OnEnter()
    {
    }

    public void OnExit()
    {
    }
}