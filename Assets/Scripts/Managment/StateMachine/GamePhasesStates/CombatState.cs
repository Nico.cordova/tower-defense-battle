﻿using UnityEngine;
using UnityEngine.AI;

internal class CombatState : IState
{
    private readonly GameObject _gameManager;


    public CombatState(GameObject gameManager)
    {
        _gameManager = gameManager;
    }
    public void Tick()
    {
    }

    public void OnEnter()
    {
    }

    public void OnExit()
    {
    }
}