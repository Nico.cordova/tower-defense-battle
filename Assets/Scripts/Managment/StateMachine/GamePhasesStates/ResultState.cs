﻿using UnityEngine;
using UnityEngine.AI;

internal class Results : IState
{
    private readonly GameObject _gameManager;

    public Results(GameObject gameManager)
    {
        _gameManager = gameManager;
    }
    public void Tick()
    {
    }

    public void OnEnter()
    {
    }

    public void OnExit()
    {
    }
}