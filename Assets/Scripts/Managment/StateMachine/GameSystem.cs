﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class GameSystem : MonoBehaviour
{
    public static GameSystem instance;
    public GameObject[] enemies;
    private StateMachine _stateMachine;

    private WaveSpawner _waveSpawner;
    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one buildmanager in scene!");
            return;
        }
        instance = this;

        _waveSpawner = gameObject.GetComponent<WaveSpawner>();

        _stateMachine = new StateMachine();

        var beginning = new Begin(gameObject);
        var wavepick = new WavePick(gameObject);
        var combat = new CombatState(gameObject);
        var results = new Results(gameObject);


        At(beginning, wavepick, BattleFieldEmpty());
        At(wavepick, combat, ChoiceLocked());
        At(combat, results, BattleFinished());
        //At(moveToSelected, search, StuckForOverASecond());
        //At(moveToSelected, harvest, ReachedResource());
        //At(harvest, search, TargetIsDepletedAndICanCarryMore());
        //At(harvest, returnToStockpile, InventoryFull());
        //At(returnToStockpile, placeResourcesInStockpile, ReachedStockpile());
        //At(placeResourcesInStockpile, search, () => _gathered == 0);
        //
        //_stateMachine.AddAnyTransition(flee, () => enemyDetector.EnemyInRange);
        //At(flee, search, () => enemyDetector.EnemyInRange == false);
        //
        //_stateMachine.SetState(search);
        //
        void At(IState to, IState from, Func<bool> condition) => _stateMachine.AddTransition(to, from, condition);
        //Func<bool> HasTarget() => () => Target != null;
        //Func<bool> StuckForOverASecond() => () => moveToSelected.TimeStuck > 1f;
        //Func<bool> ReachedResource() => () => Target != null &&
        //                                      Vector3.Distance(transform.position, Target.transform.position) < 1f;
        //
        //Func<bool> TargetIsDepletedAndICanCarryMore() => () => (Target == null || Target.IsDepleted) && !InventoryFull().Invoke();
        //Func<bool> InventoryFull() => () => _gathered >= _maxCarried;
        //Func<bool> ReachedStockpile() => () => StockPile != null &&
        Func<bool> ChoiceLocked() => () => wavepick.lockedPick;
        Func<bool> BattleFieldEmpty() => () => GameObject.FindGameObjectsWithTag("Enemy").Length == 0;
        Func<bool> BattleFinished() => () => (_waveSpawner.FinishedSpawning() && GameObject.FindGameObjectsWithTag("Enemy").Length == 0) || PlayerStats.Health <= 0;
        //Vector3.Distance(transform.position, StockPile.transform.position) < 1f;
    }

    private void Update() => _stateMachine.Tick();


}