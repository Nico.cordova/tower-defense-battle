﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class BuildingNode : MonoBehaviour
{

    public Vector3 turretOffset;
    public Vector3 guiOffset;
    public Vector3 turretGuiOffset;

    BuildManager buildManager;
    private GameObject buildingGUI;
    private GameObject turretGUI;
    public GameObject turret;

    private void Start()
    {
        buildManager = BuildManager.instance;
        turret = null;
    }
    
    public void LaunchShop()
    {

        if (turret != null)
        {
            Quaternion rotation = new Quaternion();
            turretGUI = buildManager.GetTurretGui(transform.position + turretGuiOffset, rotation);

            TurretBoardController guiController = turretGUI.GetComponent<TurretBoardController>();
            guiController.InitFromTurret(turret);

        } else
        {
            Quaternion rotation = new Quaternion();
            buildingGUI = buildManager.GetGui(transform.position + guiOffset, rotation);
            Shop shopComponent = buildingGUI.GetComponentInChildren<Shop>();
            shopComponent.SetNodeToBuildIn(gameObject);
        }
    }

    public void BuildTurret(TurretBlueprint newTurret)
    {
        Quaternion rotation = new Quaternion();
        turret = Instantiate<GameObject>(newTurret.prefab, transform.position + turretOffset, rotation);

    }

    public static bool IsPointerOverGameObject()
    {

        //check touch
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                return true;
        }

        return false;
    }

}
