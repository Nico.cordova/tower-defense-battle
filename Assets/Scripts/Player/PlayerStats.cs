﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class PlayerStats : MonoBehaviour
{
    public static PlayerStats instance;

    public static Currency Money;
    public static float Health;
    public static float Experience;
    public static int Rounds;

    public Currency startingCurrency;
    public float startingHealth;

    public Stat ExperienceToLevelUp;
    public TextMeshProUGUI FireCounter;
    public TextMeshProUGUI WaterCounter;
    public TextMeshProUGUI WindCounter;
    public TextMeshProUGUI ElectricCounter;
    public TextMeshProUGUI EarthCounter;
    public TextMeshProUGUI HealthCounter;
    public Image HealthSlider;
    public Gradient HealthGradient;
    public Image ExperienceSlider;

    void Start()
    {
        Money = startingCurrency;
        Health = startingHealth;
        Rounds = 0;
    }
    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one PlayerStat in scene!");
            return;
        }
        instance = this;
        SetSliderMaxHealth();
        SetupSliderExperience();
    }
    void Update()
    {
        FireCounter.text = Money.FireCurrency.ToString();
        WindCounter.text = Money.WindCurrency.ToString();
        WaterCounter.text = Money.WaterCurrency.ToString();
        EarthCounter.text = Money.EarthCurrency.ToString();
        ElectricCounter.text = Money.ElectricCurrency.ToString();
        HealthCounter.text = Health.ToString();
    }

    public void TakeDamage(float damage)
    {
        Debug.Log("Player Taking Damage: ");
        Debug.Log(damage);
        Debug.Log("ACTUAL HEALTH: ");
        Debug.Log(Health);
        if (Health > 0)
        {
            Debug.Log("HEALTH AFTER DAMAGE TAKEN: ");
            Debug.Log(Health);
            Health -= damage;
            RefreshHealthSlider();
        }
    }
    public void SetSliderMaxHealth()
    {
        HealthSlider.fillAmount = 1f;
        HealthSlider.color = HealthGradient.Evaluate(1f);
    }
    public void RefreshHealthSlider()
    {
        float norm_health = Health / startingHealth;
        HealthSlider.fillAmount = norm_health;
        HealthSlider.color = HealthGradient.Evaluate(norm_health);
    }

    public void SetupSliderExperience()
    {
        ExperienceSlider.fillAmount = 0f;
    }
    public void GainExperience(float experience)
    {
        if (Experience <= ExperienceToLevelUp.GetValue() * 1.3)
        {
            Experience += experience;
            RefreshExperienceSlider();
        }
    }
    public void RefreshExperienceSlider()
    {
        float norm_exp = Experience / ExperienceToLevelUp.GetValue();
        if (norm_exp > 1)
        {
            norm_exp = 1;
        }
        ExperienceSlider.fillAmount = norm_exp;
    }

    public void SpendExperience(float experience_spent)
    {
        Experience -= experience_spent;
        RefreshExperienceSlider();
    }
}
