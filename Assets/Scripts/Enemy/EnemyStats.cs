﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyStats : CharacterStats
{
    public Stat ExperienceToGive;
    public Sprite displayImage;
    private EnemyController controller;
    private CircleCollider2D circleCollider;
    private PlayerStats playerStats;
    override public void Die()
    {
        playerStats.GainExperience(ExperienceToGive.GetValue());
        Bounds bounds = circleCollider.bounds;
        AstarPath.active.UpdateGraphs(bounds);
        controller.GiveCurrency();
        Destroy(gameObject);
    }

    private void Start()
    {
        controller = GetComponent<EnemyController>();
        circleCollider = GetComponent<CircleCollider2D>();
        playerStats = PlayerStats.instance;
    }
}