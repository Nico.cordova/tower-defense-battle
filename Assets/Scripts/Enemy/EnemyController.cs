﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyController : MonoBehaviour
{
    // Start is called before the first frame update
    public Currency value;
    public Currency cost;
    public HealthBar healthBar;

    private string state;
    private GameObject attackTarget;
    private Animator anim;
    private CircleCollider2D circleCollider;
    private EnemyStats stats;

    private void Start()
    {
        state = "search";
        stats = gameObject.GetComponent<EnemyStats>();
        anim = GetComponentInChildren<Animator>();
        circleCollider = GetComponent<CircleCollider2D>();
        healthBar.SetMaxHealth(stats.maxHealth);
    }

    private void Update()
    {
        if (state == "attack" && stats.attackCooldown.GetValue() <= 0)
        {
            Hit(attackTarget);
            stats.attackCooldown.EmptyModifiers();
        }
        else
        {
            stats.attackCooldown.AddModifier(-Time.deltaTime);
        }
    }

    public void TakeDamage(float damage, ElementalDamage[] p_elementalDistribution)
    {
        int damageToTake = 0;
        foreach (ElementalDamage elementalDamage in p_elementalDistribution)
        {
            float elementalDamageToTake = elementalDamage.ratio * damage;
            switch (elementalDamage.Element)
            {
                case "Water":
                    damageToTake += (int)Math.Ceiling(elementalDamageToTake - elementalDamageToTake * stats.waterResistance.GetValue());
                    break;
                case "Fire":
                    damageToTake += (int)Math.Ceiling(elementalDamageToTake - elementalDamageToTake * stats.fireResistance.GetValue());
                    break;
                case "Electric":
                    damageToTake += (int)Math.Ceiling(elementalDamageToTake - elementalDamageToTake * stats.electricResistance.GetValue());
                    break;
                case "Wind":
                    damageToTake += (int)Math.Ceiling(elementalDamageToTake - elementalDamageToTake * stats.windResistance.GetValue());
                    break;
                case "Earth":
                    damageToTake += (int)Math.Ceiling(elementalDamageToTake - elementalDamageToTake * stats.earthResistance.GetValue());
                    break;
            }
        }

        Color32 damageColor;
        if (damageToTake >= 0) 
        { 
            if (damageToTake > damage)
            {
                damageColor = BuildManager.instance.effectiveDamageColor;
            } else if (damageToTake < damage)
            {
                damageColor = BuildManager.instance.reducedDamageColor;
            } else
            {
                damageColor = BuildManager.instance.normalDamageColor;
            }
        } else
        {
            damageColor = BuildManager.instance.healColor;
            damageToTake = -damageToTake;
        }
        healthBar.SetHealth(stats.GetCurrentHealth());
        DamagePopUp.Create(healthBar.transform.position, damageToTake, damageColor);
        float health = stats.TakeDamage(damageToTake);
        if (health <= 0)
        {
            stats.Die();
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    { 
        if (collision.gameObject.name == "UserBasePH")
        {
            state = "attack";
            anim.SetBool("isAttacking", true);
            attackTarget = collision.gameObject;
        }
       
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "UserBasePH")
        {
            state = "search";
            anim.SetBool("isAttacking", false);

            attackTarget = collision.gameObject;
        }
    }

    public void GiveCurrency()
    {
        if (value.FireCurrency >= 0)
        {
            PlayerStats.Money.FireCurrency += value.FireCurrency;
        }
        if (value.WaterCurrency >= 0)
        {
            PlayerStats.Money.WaterCurrency += value.WaterCurrency;
        }
        if (value.WindCurrency >= 0)
        {
            PlayerStats.Money.WindCurrency += value.WindCurrency;
        }
        if (value.ElectricCurrency >= 0)
        {
            PlayerStats.Money.ElectricCurrency += value.ElectricCurrency;
        }
        if (value.EarthCurrency >= 0)
        {
            PlayerStats.Money.EarthCurrency += value.EarthCurrency;
        }
    }

    void Hit(GameObject target)
    {
        CastleController targetController = target.GetComponent<CastleController>();
        targetController.DamagePlayer(stats.damage.GetValue());
    }
}
