﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyAI : MonoBehaviour
{
    public Transform target = null;
    public float nextWaypointDistance = 3f;
    public Transform enemyGfx;

    Path path;
    int currentWaypoint = 0;
    bool reachedEndOfPath = false;

    Seeker seeker;
    Rigidbody2D rb;
    CircleCollider2D circleCollider;

    private EnemyStats stats;
    private Bounds previousBounds;
    // Start is called before the first frame update
    void Start()
    {
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();
        circleCollider = GetComponent<CircleCollider2D>();
        if (target != null)
        {
            InvokeRepeating("UpdatePath", 0f, 1f);
            seeker.StartPath(rb.position, target.position, OnPathComplete);
        }
        stats = GetComponent<EnemyStats>();
        StartCoroutine(updateGraph());
    }

    IEnumerator updateGraph()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            Bounds bounds = circleCollider.bounds;
            AstarPath.active.UpdateGraphs(bounds);
            AstarPath.active.UpdateGraphs(previousBounds);
            previousBounds = bounds;
        }
    }
    void UpdatePath()
    {
        if (seeker.IsDone())
        {
            seeker.StartPath(rb.position, target.position, OnPathComplete);
        }
    }

    void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (path == null)
            return;

        if (currentWaypoint >= path.vectorPath.Count)
        {
            reachedEndOfPath = true;
        }
        else
        {
            reachedEndOfPath = false;
        }

        if (!reachedEndOfPath)
        {
            Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
            Vector2 force = direction * stats.moveSpeed.GetValue() * Time.deltaTime;

            float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);

            if (distance < nextWaypointDistance)
            {
                currentWaypoint++;
            }
            if (rb.velocity.x >= 1f && enemyGfx.localScale.x < 0)
            {
                enemyGfx.localScale = new Vector3(Mathf.Abs(enemyGfx.localScale.x), enemyGfx.localScale.y, enemyGfx.localScale.z);

            }
            else if (rb.velocity.x <= -1f && enemyGfx.localScale.x > 0)
            {
                enemyGfx.localScale = new Vector3(-enemyGfx.localScale.x, enemyGfx.localScale.y, enemyGfx.localScale.z);

            }
            rb.AddForce(force);
        }

    }
}
